<?php
include "modulos/perfil.php";
include 'templates/cabeceraUsuario.php';
include 'templates/sidebar.php';
include "modulos/selectcategorias.php";
include "modulos/regiones.php";

?>
<div class="content-wrapper" style="padding: 1%"> 
	<section class="content-header">
		<h1>
			Publicaciones
		</h1>
	</section>
	<form class="form-inline ml-3" action="perfil.php" method="post" enctype="multipart/form-data" >
		<div class="col-xl-2">
			<button type="submit" name="btnPerfil" id="thesubmitBoton" class="btn btn-primary btn-block">Actualizar datos</button>
		</div>	
		<div class="row">
        	<div class="col-4">
                <div class="card tarjeta"> 
                    <div class="card-body det">
                        <!-- Nombre Usuario -->                                               
                        <div class="input-group mb-3">
                            <input minlength="4" maxlength="13" type="text" name="txtNombre" class="form-control" value="<?php  echo $_SESSION['usuario']['Nombres']?>">                   
                        </div>        
                        <!-- Apellido Usuario -->
                        <div class="input-group mb-3">
							<input minlength="4" maxlength="13" type="text" name="txtApellido" class="form-control" value="<?php  echo $_SESSION['usuario']['Apellidos']?>">                                               
                        </div>                    
                        <!-- Rut -->
						<div class="input-group mb-3">
							<input type="text" name="txtRut" id="txtRut" class="form-control" placeholder="RUT"  value="<?php  echo $_SESSION['usuario']['RUT']?>"/>         
						</div>						
						<!-- Telefono -->
						<div class="input-group mb-3">
							<input minlength="8" maxlength="13" type="tel" name="txtPhono" id="txtPhono" class="form-control" value="<?php  echo $_SESSION['usuario']['Telefono']?>" pattern="[0-9+]"/>
						</div>
						<!-- Email -->       
						<div class="input-group mb-3">
							<input type="email" name="txtCorreo" id="txtCorreo" class="form-control" value="<?php  echo $_SESSION['usuario']['Email']?>" disabled>
						</div>        
                    </div>
                </div>
            </div>            
            <div class="col-4">
                <div class="card tarjeta"> 
                    <div class="card-body det">
                        <!--Regiones  -->
                            <div class="input-group mb-3">
                            <select name="cbxRegion" id="cbxRegion" class="form-control" required>
                                <option selected value="0"> Region </option>
                                <?php while ($region = $reg->fetch(PDO::FETCH_ASSOC)) {?>
                                <option value="<?php echo $region['idRegion']; ?>"><?php echo $region['region']; ?></option>
                                <?php } ?>
                            </select>
                            </div>                                
                            <!-- Comunas  -->
                            <div class="input-group mb-3">
            	                <select name="cbxComuna" id="cbxComuna" class="form-control" require></select>
							</div>
							<!-- Contraseña  -->
							<div class="input-group mb-3">
								<input type="password" name="txtPass" id="txtPass" class="form-control" placeholder="Contraseña" required />
							</div>
							<!-- Repite Contraseña  -->
							<div class="input-group mb-3">
								<input type="password" name="txtRetypePass" id="txtRetypePass" class="form-control" placeholder="Repetir Contraseña" required>
							</div>
                             <!-- SUBIR IMAGEN -->
							 <table>
                                <tr>
                                    <td>
                                        <label for="imagen"></label>
                                    </td>
                                    <td>
                                        <input type="file" name="imagen" size="20">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align:center">
                                        <input type="submit" value="Enviar imagen">
                                    </td>
                                </tr>    
							</table>
                    </div>
                </div>            
			</div>
			<div class="col-4">
                <div class="card tarjeta"> 
                    <div class="card-body det"> 
                        <!-- Contraseña  -->
						<div class="input-group mb-3">
							<input type="password" name="txtPass" id="txtPass" class="form-control" placeholder="Contraseña" required />
						</div>
						<!-- Repite Contraseña  -->
						<div class="input-group mb-3">
							<input type="password" name="txtRetypePass" id="txtRetypePass" class="form-control" placeholder="Repetir Contraseña" onBlur="comprobarContraseña()" required>
						</div>	
                    </div>
                </div>            
            </div>
		</div>
	</form>
<?php
    include 'templates/piePagina.php';
?>