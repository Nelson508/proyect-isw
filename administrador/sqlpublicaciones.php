<?php
class sql extends DB {
	public function __construct()
	{
		parent::__construct();
	}

	public function list_publicaciones()
	{
		$db = $this->connect();
		try
		{
			$stmt = $db->prepare("SELECT P.IDPublicacion AS idpubli,P.Tipo_publicacion AS tipo,
										 P.Fecha_publicacion AS fecha_pu,
										 P.Fecha_ult_estado AS fecha_estado,U.Nombres AS nom,
										 E.Nombre_estado AS estado,D.NombreProducto AS producto 
								  FROM publicacion P,usuarios U,estado E, producto D 
								  WHERE U.IDUsuario = P.IDUsuario AND P.IDEstado=E.IDEstado 
								  AND P.IDPublicacion=D.IDPublicacion");
			
			$stmt->execute();
			$stat[0] = true;
			$stat[1] = "List categoria";
			$stat[2] = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $stat;
		}
		catch(PDOException $ex)
		{
			$stat[0] = false;
			$stat[1] = $ex->getMessage();
			$stat[2] = [];
			return $stat;
		}
	}

	public function delete_customer($id)
	{
		$db = $this->connect();
		try
		{
			$stmt = $db->prepare("DELETE FROM publicacion WHERE IDPublicacion = :id");
			$stmt->bindParam("id",$id);
			$stmt->execute();
			$stat[0] = true;
			$stat[1] = "Success delete customer";
			return $stat;
		}
		catch(PDOException $ex)
		{
			$stat[0] = false;
			$stat[1] = $ex->getMessage();
			return $stat;
		}
	}

}

?>