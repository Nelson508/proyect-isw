<?php 

include "../global/conexion.php";
include "sqlcategorias.php";
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$method=$_POST['method'];
	$dtbs = new sql();
	$retval = [];

	if($method == 'list_categoria'){
		$list = $dtbs->list_categoria();
		$retval['status'] = $list[0];
		$retval['message'] = $list[1];
		$retval['data'] = $list[2];
		echo json_encode($retval);
	}

	if($method == 'new_customer'){
		$name = $_POST['name'];
		$padre = $_POST['padre'];

		$new = $dtbs->new_customer($name,$padre);
		$retval['status'] = $new[0];
		$retval['message'] = $new[1];
		echo json_encode($retval);
	}

	if($method == 'edit_customer'){
		$idcat = $_POST['IDCategoria'];
		$name = $_POST['Nombre_categoria'];
		$padre = $_POST['IDPadre'];

		$edit = $dtbs->edit_customer($idcat,$name,$padre);
		$retval['status'] = $edit[0];
		$retval['message'] = $edit[1];
		echo json_encode($retval);
	}

	if($method == 'delete_customer'){
		$idcat = $_POST['IDCategoria'];
		$delete = $dtbs->delete_customer($idcat);
		$retval['status'] = $delete[0];
		$retval['message'] = $delete[1];
 		echo json_encode($retval);
	}



}else{
	header("HTTP/1.1 401 Unauthorized");
    exit;
}