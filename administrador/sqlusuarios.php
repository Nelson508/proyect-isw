<?php
class sql extends DB {
	public function __construct()
	{
		parent::__construct();
	}

	public function list_usuario()
	{
		$db = $this->connect();
		try
		{
			$stmt = $db->prepare("SELECT U.IDUsuario AS iduser,U.Tipo_Usuario AS rol, U.Nombres AS nombre, 
										 U.Apellidos AS apellido, U.RUT AS rut, U.Telefono AS tel, 
										 U.Email AS email, C.comuna AS comuna, R.region AS region 
								  FROM usuarios U, comuna C, regiones R WHERE U.Tipo_Usuario = 'Persona' 
								  AND U.idComuna= C.idComuna AND C.idRegion = R.idRegion");
			
			$stmt->execute();
			$stat[0] = true;
			$stat[1] = "List categoria";
			$stat[2] = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $stat;
		}
		catch(PDOException $ex)
		{
			$stat[0] = false;
			$stat[1] = $ex->getMessage();
			$stat[2] = [];
			return $stat;
		}
	}

	public function edit_customer($id,$name,$padre)
	{
		$db = $this->connect();
		try
		{
			$stmt = $db->prepare("UPDATE categoria SET Nombre_categoria = :name, IDPadre = :padre WHERE IDCategoria = :id ");
			$stmt->bindParam("id",$id);
			$stmt->bindParam("name",$name);
			$stmt->bindParam("padre",$padre);
			$stmt->execute();
			$stat[0] = true;
			$stat[1] = "Success edit customer";
			return $stat;
		}
		catch(PDOException $ex)
		{
			$stat[0] = false;
			$stat[1] = $ex->getMessage();
			return $stat;
		}
	}

	public function delete_customer($id)
	{
		$db = $this->connect();
		try
		{
			$stmt = $db->prepare("DELETE FROM usuarios WHERE IDUsuario = :id");
			$stmt->bindParam("id",$id);
			$stmt->execute();
			$stat[0] = true;
			$stat[1] = "Success delete customer";
			return $stat;
		}
		catch(PDOException $ex)
		{
			$stat[0] = false;
			$stat[1] = $ex->getMessage();
			return $stat;
		}
	}

}

?>