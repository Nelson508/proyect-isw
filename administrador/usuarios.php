<?php 

include "../global/conexion.php";
include "sqlusuarios.php";
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$method=$_POST['method'];
	$dtbs = new sql();
	$retval = [];

	if($method == 'list_usuario'){
		$list = $dtbs->list_usuario();
		$retval['status'] = $list[0];
		$retval['message'] = $list[1];
		$retval['data'] = $list[2];
		echo json_encode($retval);
	}

	if($method == 'edit_customer'){
		$idcat = $_POST['IDCategoria'];
		$name = $_POST['Nombre_categoria'];
		$padre = $_POST['IDPadre'];

		$edit = $dtbs->edit_customer($idcat,$name,$padre);
		$retval['status'] = $edit[0];
		$retval['message'] = $edit[1];
		echo json_encode($retval);
	}

	if($method == 'delete_customer'){
		$iduser = $_POST['iduser'];
		$delete = $dtbs->delete_customer($iduser);
		$retval['status'] = $delete[0];
		$retval['message'] = $delete[1];
 		echo json_encode($retval);
	}



}else{
	header("HTTP/1.1 401 Unauthorized");
    exit;
}