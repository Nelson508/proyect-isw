<?php
class sql extends DB {
	public function __construct()
	{
		parent::__construct();
	}

	public function new_customer($name,$padre)
	{
		$db = $this->connect();
		try
		{
			$stmt = $db->prepare("INSERT INTO categoria(IDCategoria,Nombre_categoria,IDPadre) VALUES (NULL,:name_cat,:IDP)");
			$stmt->bindParam("name_cat",$name);
			$stmt->bindParam("IDP",$padre);
			$stmt->execute();
			$stat[0] = true;
			$stat[1] = "Success save customer";
			return $stat;
		}
		catch(PDOException $ex)
		{
			$stat[0] = false;
			$stat[1] = $ex->getMessage();
			return $stat;
		}
	}

	public function list_categoria()
	{
		$db = $this->connect();
		try
		{
			//$stmt = $db->prepare("SELECT * FROM categoria");
			/*$stmt = $db->prepare("SELECT @pv:=(SELECT GROUP_CONCAT(IDPadre SEPARATOR ',') FROM categoria 
			WHERE FIND_IN_SET(IDCategoria, @pv)) AS lv FROM categoria 
			JOIN
			(SELECT @pv:=2) tmp");*/
			//$stmt = $db->prepare("select @pv:=IDCategoria as IDCategoria, Nombre_categoria, IDPadre from categoria join (select @pv:=0)tmp where IDPadre=@pv");
			//$stmt = $db->prepare("SELECT A.IDCategoria,A.Nombre_categoria,A.IDPadre,N.Nombre_categoria FROM categoria A INNER JOIN categoria N WHERE A.IDPadre = N.IDCategoria");
			$stmt = $db->prepare("SELECT A.IDCategoria AS idcat,A.Nombre_categoria as nombre_cat,A.IDPadre AS idpa,N.Nombre_categoria as nombre_padre FROM categoria A INNER JOIN categoria N 	WHERE A.IDPadre = N.IDCategoria
			UNION ALL
			SELECT IDCategoria AS idcat,Nombre_categoria as nombre_cat,IDPadre AS idpa,Nombre_categoria as nombre_padre FROM categoria WHERE IDPadre = 0
			ORDER BY idcat");
			
			$stmt->execute();
			$stat[0] = true;
			$stat[1] = "List categoria";
			$stat[2] = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $stat;
		}
		catch(PDOException $ex)
		{
			$stat[0] = false;
			$stat[1] = $ex->getMessage();
			$stat[2] = [];
			return $stat;
		}
	}

	public function edit_customer($id,$name,$padre)
	{
		$db = $this->connect();
		try
		{
			$stmt = $db->prepare("UPDATE categoria SET Nombre_categoria = :name, IDPadre = :padre WHERE IDCategoria = :id ");
			$stmt->bindParam("id",$id);
			$stmt->bindParam("name",$name);
			$stmt->bindParam("padre",$padre);
			$stmt->execute();
			$stat[0] = true;
			$stat[1] = "Success edit customer";
			return $stat;
		}
		catch(PDOException $ex)
		{
			$stat[0] = false;
			$stat[1] = $ex->getMessage();
			return $stat;
		}
	}

	public function delete_customer($id)
	{
		$db = $this->connect();
		try
		{
			$stmt = $db->prepare("DELETE FROM categoria WHERE IDCategoria = :id");
			$stmt->bindParam("id",$id);
			$stmt->execute();
			$stat[0] = true;
			$stat[1] = "Success delete customer";
			return $stat;
		}
		catch(PDOException $ex)
		{
			$stat[0] = false;
			$stat[1] = $ex->getMessage();
			return $stat;
		}
	}

}

?>