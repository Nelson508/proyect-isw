<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index.php" class="brand-link">
    <img src="dist/img/logo1.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
    <span class="brand-text font-weight-light">Gift.com</span>
  </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <a href="admin.php" class="nav-link">
              <i class="nav-icon fas fa-list-alt"></i>
              <p>
                Ver Categorias
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="usuarios.php" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Ver Usuarios
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="publicaciones.php" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Ver Publicaciones
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Ver Reportes
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>