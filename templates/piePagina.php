  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy;<a href="#"> www.gift.com </a>| 2020</strong> 
  </footer>

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>  
<!-- AdminLTE App 
<script src="dist/js/adminlte.min.js"></script>-->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Modal -->
<script src="dist/js/modal.js"></script>
<!-- Administrador -->
<!-- jQuery 3 
  <script src="bower_components/jquery/dist/jquery.min.js"></script>-->
  <!-- Bootstrap 3.3.7 
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>-->
  <!-- SlimScroll 
  <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>-->
  <!-- FastClick 
  <script src="bower_components/fastclick/lib/fastclick.js"></script>-->
  <!-- AdminLTE App
  <script src="dist/js/adminlte.min.js"></script> -->
<!-- Categorias
<script src="dist/js/categorias.js"></script> -->

<!-- Validación-->
<script src="dist/js/validacion.js"></script>

	<script src="plugins/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

  </body>
</html>