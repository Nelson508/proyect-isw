<?php
include 'global/config.php';
include "global/sesiones.php";
include 'global/conexion.php';
include 'templates/cabeceraUsuario.php';
include 'templates/sidebar.php';
include "modulos/selectcategorias.php";
include "modulos/regiones.php";
?>
    <div class="content-wrapper" style="padding: 1%">
        <div class="row">
            <div class="row col-9">
                    <?php
                    $response = json_decode(file_get_contents('http://localhost/proyect-isw/productos/api-productos.php?Elementos=verdadero'),true); 
                        if($response['statuscode'] == 200){
                            foreach($response['items'] as $item){
                                include('layout/items.php');
                            }
                        }
                        ?>
            </div>
            <div class="col-3">
                <div class="card tarjeta2"> 
                    <div class="card-body det">
                        <h5 class="card-title"><span><b>BUSCADOR</b></span></h5>
                        <!-- Categorias  -->
                        <div class="row">
                            <form class="form-inline ml-3" action="categorias.php" method="post">
                                <div class="input-group input-group-sm">
                                <select name="cbxCategoria" id="cbxCategoria" class="form-control" required>
                                    <option selected value="0"> Categoria </option>
                                    <?php while ($categ = $query->fetch(PDO::FETCH_ASSOC)) {?>
                                    <option value="<?php echo $categ['IDCategoria']; ?>"><?php echo $categ['Nombre_categoria']; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="input-group-append">
                                <button class="btn btn-navbar" type="submit" name="btnBusCat" id="btnBusCat" >
                                    <i class="fas fa-search"></i>
                                </button>
                                </div>
                                </div>
                            </form>
                            <!-- SEARCH FORM -->
                            <form class="form-inline ml-3">
                                <div class="input-group input-group-sm buscadores">
                                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                                    <div class="input-group-append">
                                    <button class="btn btn-navbar" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                    </div>
                                </div>
                            </form>
                            <form>
                                <!-- Regiones  -->
                                <div class="input-group mb-3 buscadores2">
                                <select name="cbxRegion" id="cbxRegion" class="form-control" required>
                                    <option selected value="0"> Region </option>
                                    <?php while ($region = $reg->fetch(PDO::FETCH_ASSOC)) {?>
                                    <option value="<?php echo $region['idRegion']; ?>"><?php echo $region['region']; ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                                <!-- Comunas  -->
                                <div class="input-group mb-3 buscadores2">
                                <select name="cbxComuna" id="cbxComuna" class="form-control" require></select>
                                </div>
                            </form>
                            <div class="pal">
                                <button type="button" class="btn btn-primary">
                                    Ver detalles
                                </button>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div>    
    
<?php
    include 'templates/piePagina.php';
?>

