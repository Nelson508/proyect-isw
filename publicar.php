<?php
include "modulos/publicar.php";
include 'templates/cabeceraUsuario.php';
include 'templates/sidebar.php';
include "modulos/selectcategorias.php";
include "modulos/regiones.php";

?>
<div class="content-wrapper" style="padding: 1%">
    
        <div class="row">
        <form class="form-inline ml-3" action="publicar.php" method="post" enctype="multipart/form-data" >
        <div class="col-5 offset-7">
            <button type="submit" name="btnPublica" id="thesubmitBoton" class="btn btn-primary btn-block">Publicar</button>
          </div>
        
        <div class="col-6">
                <div class="card tarjeta"> 
                    <div class="card-body det">
                        <h5 class="card-title"><span><b>Informacion Producto</b></span></h5>
                        <!-- Nombre Producto -->                       
                        
                            <div class="input-group mb-3 buscadores2">
                                <input minlength="4" maxlength="13" type="text" name="txtNombre" class="form-control" placeholder="Nombre Producto" required>                   
                            </div>
                        
                        <!-- Descripcion Producto -->
                        
                            <div class="input-group mb-3 buscadores2">
                                <textarea class="form-control" name="txtaDescripcion" rows="3" placeholder="Descripcion" ></textarea>                                               
                            </div>
                                    
                        <!-- Categorias  -->
                         
                            <div class="input-group input-group-sm buscadores2">
                                <select name="cbxCatego" id="cbxCatego" class="form-control" required>
                                    <option selected value="0"> Categoria </option>
                                    <?php while ($categ = $query->fetch(PDO::FETCH_ASSOC)) {?>
                                    <option value="<?php echo $categ['IDCategoria']; ?>"><?php echo $categ['Nombre_categoria']; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="input-group-append">
                                    <button class="btn btn-navbar" type="submit" name="btnBusCat" id="btnBusCat" >
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        
                            <!-- SUBIR IMAGEN -->
                         
                            <div class="form-group buscadores2">
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile"  name="imagen[]" size="20" multiple>  
                                        <label class="custom-file-label" for="exampleInputFile">Seleccionar imagen</label>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>            
            </div>
            <div class="col-6">
                <div class="card tarjeta"> 
                    <div class="card-body det">
                        <h5 class="card-title"><span><b>Informacion publicacion</b></span></h5>           
                            <!-- Regiones  -->
                                <div class="input-group mb-3 buscadores2">
                                <select name="cbxRegion" id="cbxRegion" class="form-control" required>
                                    <option selected value="0"> Region </option>
                                    <?php while ($region = $reg->fetch(PDO::FETCH_ASSOC)) {?>
                                    <option value="<?php echo $region['idRegion']; ?>"><?php echo $region['region']; ?></option>
                                    <?php } ?>
                                </select>
                                </div>                                
                                <!-- Comunas  -->
                                <div class="input-group mb-3 buscadores2">
                                    <select name="cbxComuna" id="cbxComuna" class="form-control" require></select>
                                </div>
                                <!-- Datos usuario  -->
                                <div class="input-group buscadores3">
                                    <label>Nombre:<span class="text-muted"><?php  echo $_SESSION['usuario']['Nombres'] . ' ' . $_SESSION['usuario']['Apellidos']?></span></label>                                      
                                </div>
                                <div class="input-group buscadores3">
                                    <label>Correo:<span class="text-muted"><?php  echo $_SESSION['usuario']['Email']?></span></label>              
                                </div>
                                <div class="input-group buscadores3">
                                    <label>Telefono:<span class="text-muted"><?php  echo $_SESSION['usuario']['Telefono']?></span></label>                                                    
                                </div>
                            
                        
                    </div>
                </div>            
            </div>
        </form>
        </div>
<?php
    include 'templates/piePagina.php';
?>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>