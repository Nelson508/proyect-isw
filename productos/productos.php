<?php

include_once '../global/conexion.php';

class Productos extends DB{

    function __construct()
    {
        parent::__construct();
    }

    public function get(){

        $query = $this->connect()->prepare("SELECT * FROM producto");
        //$query = $this->connect()->prepare("SELECT * FROM producto A, imagen B WHERE  A.IDProducto = B.IDProducto");
        $query->execute();

        $row = $query->fetchAll(PDO::FETCH_ASSOC);

        return $row;
    }

    public function getItemsByCategory($idcategory){

        $query = $this->connect()->prepare("SELECT * FROM producto
                                                      WHERE IDCategoria = :idcat");


        $query->execute(['idcat'=>$idcategory]);

        $items = [];

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

            $item = [
                'IDProducto'     => $row['IDProducto'],
                'IDCategoria'      => $row['IDCategoria'],
                'NombreProducto' => $row['NombreProducto'],              
                'Descripcion'    => $row['Descripcion'],
                ];

                array_push($items,$item);
            
        }

        return $items;

        
    }

    /*public function crear_menu($id_padre) { 



        $consulta = $this->connect()->prepare("SELECT * FROM categoria WHERE IDPadre=:pad");
        $consulta->execute(['pad'=>$id_padre]);

        $categorias = [];

        while ($row = $consulta->fetch(PDO::FETCH_ASSOC)) {

            $categoria = [
                'IDCategoria'       => $row['IDCategoria'],
                'Nombre_categoria'  => $row['Nombre_categoria'],
                'IDPadre'           => $row['IDPadre'],
                'link'              => $row['link'],
                ];

                array_push($categorias,$categoria);
            
        }

        return $categorias;
      }

*/

}



?>