<?php

include_once 'productos.php';

if(isset($_GET['IDCategoria'])){
    $idcategoria = $_GET['IDCategoria'];

    if($idcategoria == ''){
        echo json_encode(['statuscode' => 400, 'response' => 'No existe esa categoria']);
    }else{
        $productos = new Productos();
        $items = $productos->getItemsByCategory($idcategoria);

        echo json_encode(['statuscode' => 200, 'items' => $items]);
    }
}elseif($_GET['Elementos'] == 'verdadero'){
    
    $categoria = $_GET['Elementos'];

    if($categoria==''){
        echo json_encode(['statuscode' => 400, 'response' => 'No hay productos']);
    }else{
        $productos = new Productos();
        $items = $productos->get();

        echo json_encode(['statuscode' => 200, 'items' => $items]);
    }

}/*elseif(isset($_GET['cate'])){

    //$cate = $_GET['cate'];

    if($cate==''){
        echo json_encode(['statuscode' => 400, 'response' => 'No existe esa categoria']);
    }else{
        $producto = new Productos();
        $categorias = $producto->crear_menu(0);

        echo json_encode(['statuscode' => 200, 'categorias' => $categorias]);
    }
 
}*/else{
    echo json_encode(['statuscode' => 400, 'response' => 'No hay accion']);

}

?>