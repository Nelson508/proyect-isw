<?php
include 'global/config.php';
include "global/sesiones.php";
include 'global/conexion.php';

if(isset($_POST['btnPublica'])){

$conne = new DB;
$conectar = $conne->connect();
$iduser = $_SESSION['usuario']['IDUsuario'];

$publicacion =  $conectar->prepare("INSERT INTO publicacion(IDUsuario,IDEstado,Tipo_publicacion,Fecha_publicacion,Fecha_ult_estado) 
                                            VALUES (:iduser,1,'Publicacion',now(),now())");
                $publicacion->bindParam("iduser",$iduser,PDO::PARAM_INT);
                $publicacion->execute();
                $lastIdpublicacion = $conectar->lastInsertId();        

$IDCatego = ($_POST['cbxCatego']);
$Nombr = ($_POST['txtNombre']);
$Descripcion = ($_POST['txtaDescripcion']);

$producto =  $conectar->prepare("INSERT INTO producto(IDProducto,IDPublicacion,IDCategoria,IDCalidad,NombreProducto,Descripcion) 
                                         VALUES (Null,:idpubli,:idcat,1,:nom,:descrip)");
                $producto->bindParam("idpubli",$lastIdpublicacion,PDO::PARAM_INT);
                $producto->bindParam("idcat",$IDCatego,PDO::PARAM_INT);
                //$producto->bindParam("idcal",$IDCalidad);
                $producto->bindParam("nom",$Nombr,PDO::PARAM_STR);
                $producto->bindParam("descrip",$Descripcion,PDO::PARAM_STR);
                $producto->execute();
                $lastIdproducto = $conectar->lastInsertId();

                $reporte = null;
                
                for($x=0; $x<count($_FILES['imagen']['name']); $x++)
                {
                    $file = $_FILES['imagen'];
                    $nombre_imagen=$file['name'][$x];
                    $tipo_imagen=$file['type'][$x];
                    $tamano_imagen=$file['size'][$x];
                    $ruta_provisional = $file["tmp_name"][$x];
                    $dimensiones = getimagesize($ruta_provisional);
                    $width = $dimensiones[0];
                    $height = $dimensiones[1];
                    $carpeta_destino=$_SERVER['DOCUMENT_ROOT'].'/proyect-isw/dist/img/pruebas/';

                    if ($tipo_imagen != 'image/jpeg' && $tipo_imagen != 'image/jpg' && $tipo_imagen != 'image/png')
                    {
                        $reporte .= "<p style='color: red'>Error $nombre_imagen, el archivo no es una imagen.</p>";
                    }
                    else if($tamano_imagen > 1024*1024)
                    {
                        $reporte .= "<p style='color: red'>Error $nombre_imagen, el tamaño máximo permitido es 1mb</p>";
                    }
                    else if($width > 800 || $height > 800)
                    {
                        $reporte .= "<p style='color: red'>Error $nombre_imagen, la anchura y la altura máxima permitida es de 500px</p>";
                    }
                    else if($width < 60 || $height < 60)
                    {
                        $reporte .= "<p style='color: red'>Error $nombre_imagen, la anchura y la altura mínima permitida es de 60px</p>";
                    }
                    else
                    {
                        move_uploaded_file($ruta_provisional,$carpeta_destino.$nombre_imagen);

                        $imagen = $conectar->prepare("INSERT INTO imagen(IDImagen,IDProducto,nombre_img) 
                                                      VALUES (NULL,:idproduc,:name_img)");
                        $imagen->bindParam("idproduc",$lastIdproducto);
                        $imagen->bindParam("name_img",$nombre_imagen);
                        $imagen->execute();
                    }
                }
}

?>