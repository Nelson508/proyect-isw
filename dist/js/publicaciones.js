$(document).ready(function(){

    $('#table-customer').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "processing": true,
        "ordering": false,
        "info": false,
        "responsive": true,
        "autoWidth": false,
        "pageLength": 100,
        "dom": '<"top"f>rtip',
        "fnDrawCallback": function( oSettings ) {
        },
        "ajax": {
            "url": "administrador/publicaciones.php",
            "type": "POST",
            "data" : {
                method : "list_publicaciones"
            },
            error: function (request, textStatus, errorThrown) {
                swal(request.responseJSON.message);
            }
        },

        columns: [
        { "data": null,render :  function ( data, type, full, meta ) {
            return  meta.row + 1;
        }},
        { "data": "tipo" },
        { "data": "producto" },
        { "data": "nom" },
        { "data": "estado" },
        { "data": "fecha_pu" },
        { "data": "fecha_estado" },
        { "data": null, render : function(data,type, full, meta){
            return "<button title='Delete' class='btn btn-hapus  btn-danger btn-xs'><i class='fa fa-remove'></i> Borrar</button> ";
        } 		},
        ]
    });

   
});

$(document).on("click",".btn-hapus",function(){
    let current_row = $(this).parents('tr'); 
    if (current_row.hasClass('child')) { 
        current_row = current_row.prev(); 
    }
    let table = $('#table-customer').DataTable(); 
    let data = table.row( current_row).data();
    let idpublicacion = data.idpubli;
    var pad = $("#txtpadre").val(); 
    swal({
        title: "Eliminar",
        text: "¿Desea eliminar la categoria?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function(){

        let ajax = {
            method : "delete_customer",
            idpubli : idpublicacion,
        }
        $.ajax({
            url:"administrador/publicaciones.php",
            type: "POST",
            data: ajax,
            success: function(data, textStatus, jqXHR)
            {
                $resp = JSON.parse(data);
                if($resp['status'] == true){
                    swal("Success delete customer");
                    let xtable = $('#table-customer').DataTable(); 
                    xtable.ajax.reload( null, false );
                }else{
                    swal("Error delete customer : "+$resp['message'])
                }
                
            },
            error: function (request, textStatus, errorThrown) {
                swal("Error ", request.responseJSON.message, "error");
            }
        });
    });
});