$(document).ready(function(){

    $('#table-customer').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "processing": true,
        "ordering": false,
        "info": false,
        "responsive": true,
        "autoWidth": false,
        "pageLength": 100,
        "dom": '<"top"f>rtip',
        "fnDrawCallback": function( oSettings ) {
        },
        "ajax": {
            "url": "administrador/usuarios.php",
            "type": "POST",
            "data" : {
                method : "list_usuario"
            },
            error: function (request, textStatus, errorThrown) {
                swal(request.responseJSON.message);
            }
        },

        columns: [
        { "data": null,render :  function ( data, type, full, meta ) {
            return  meta.row + 1;
        }},
        { "data": "nombre" },
        { "data": "rut" },
        { "data": "tel" },
        { "data": "email" },
        { "data": "comuna" },
        { "data": "region" },
        { "data": null, render : function(data,type, full, meta){
            return "<button title='Edit' class='btn btn-edit btn-warning btn-xs'><i class='fa fa-pencil'></i> Ban</button>  <button title='Delete' class='btn btn-hapus  btn-danger btn-xs'><i class='fa fa-remove'></i> Borrar</button> ";
        } 		},
        ]
    });

   
});


$(document).on("click",".btn-edit",function(){
    var current_row = $(this).parents('tr'); 
    if (current_row.hasClass('child')) { 
        current_row = current_row.prev(); 
    }
    var table = $('#table-customer').DataTable(); 
    var data = table.row( current_row).data();
    $("#txtname").val(data.nombre_cat);
    $("#txtcode").val(data.idcat);
    $("#txtpadre").val(data.idpa)
    $("#modal-customer").modal("show");
    setTimeout(function(){ 
        $("#txtname").focus()
    }, 1000);

    $("#crud").val("E");

});

$(document).on("click",".btn-hapus",function(){
    let current_row = $(this).parents('tr'); 
    if (current_row.hasClass('child')) { 
        current_row = current_row.prev(); 
    }
    let table = $('#table-customer').DataTable(); 
    let data = table.row( current_row).data();
    let idusuario = data.iduser;
    var pad = $("#txtpadre").val(); 
    swal({
        title: "Eliminar",
        text: "¿Desea eliminar la categoria?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function(){

        let ajax = {
            method : "delete_customer",
            iduser : idusuario,
        }
        $.ajax({
            url:"administrador/usuarios.php",
            type: "POST",
            data: ajax,
            success: function(data, textStatus, jqXHR)
            {
                $resp = JSON.parse(data);
                if($resp['status'] == true){
                    swal("Success delete customer");
                    let xtable = $('#table-customer').DataTable(); 
                    xtable.ajax.reload( null, false );
                }else{
                    swal("Error delete customer : "+$resp['message'])
                }
                
            },
            error: function (request, textStatus, errorThrown) {
                swal("Error ", request.responseJSON.message, "error");
            }
        });
    });
});


function edit_customer(id,nm,pad){
    let ajax = {
        method: "edit_customer",
        iduser :  id,
        Nombre_categoria : nm,
        IDPadre: pad

    }
    $.ajax({
        url:  "administrador/usuarios.php",
        type: "POST",
        data: ajax,
        success: function(data, textStatus, jqXHR)
        {
            $resp = JSON.parse(data);

            if($resp['status'] == true){
                $("#modal-customer").modal("hide");
                swal("Success edit customer ");
                let xtable = $('#table-customer').DataTable(); 
                xtable.ajax.reload( null, false );
            }else{
                swal("Error save customer : "+$resp['message'])
            }
        },
        error: function (request, textStatus, errorThrown) {
            swal("Error ", request.responseJSON.message, "error");

        }
    });
}