$(document).ready(function(){

    $('#table-customer').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "processing": true,
        "ordering": false,
        "info": false,
        "responsive": true,
        "autoWidth": false,
        "pageLength": 100,
        "dom": '<"top"f>rtip',
        "fnDrawCallback": function( oSettings ) {
        },
        "ajax": {
            "url": "administrador/api-categorias.php",
            "type": "POST",
            "data" : {
                method : "list_categoria"
            },
            error: function (request, textStatus, errorThrown) {
                swal(request.responseJSON.message);
            }
        },

        columns: [
        { "data": null,render :  function ( data, type, full, meta ) {
            return  meta.row + 1;
        }},
        { "data": "nombre_cat" },
        { "data": "idpa" },
        { "data": "nombre_padre" },
        { "data": null, render : function(data,type, full, meta){
            return "<button title='Edit' class='btn btn-edit btn-warning btn-xs'><i class='fa fa-pencil'></i> Editar</button>  <button title='Delete' class='btn btn-hapus  btn-danger btn-xs'><i class='fa fa-remove'></i> Borrar</button> ";
        } 		},
        ]
    });

    $("#btn-save").click(function(){
        if($("#txtname").val() == ''){
            swal("Introduzca un Nombre para la categoria");
            return;
        }
        if($("#txtpadre").val() == ''){
            swal("Introduzca ID de la categoria padre");
            return;
        }


        if($("#crud").val() == 'N'){
            swal({
                title: "Nuevo",
                text: "¿Crear una nueva categoria ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-primary",
                confirmButtonText: "Guardar",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){

                add_customer($("#txtname").val(),$("#txtpadre").val());
            });
        }else{
            swal({
                title: "Editar",
                text: "¿Editar categoria ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-primary",
                confirmButtonText: "Actualizar",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                edit_customer($("#txtcode").val(),$("#txtname").val(),$("#txtpadre").val());

            });
        }
    });

    $("#btn-add").click(function(){
        $("#modal-customer").modal("show");
        $("#txtname").val("");
        $("#txtpadre").val("");
        $("#crud").val("N");
    });
});



$(document).on("click",".btn-edit",function(){
    var current_row = $(this).parents('tr'); 
    if (current_row.hasClass('child')) { 
        current_row = current_row.prev(); 
    }
    var table = $('#table-customer').DataTable(); 
    var data = table.row( current_row).data();
    $("#txtname").val(data.nombre_cat);
    $("#txtcode").val(data.idcat);
    $("#txtpadre").val(data.idpa)
    $("#modal-customer").modal("show");
    setTimeout(function(){ 
        $("#txtname").focus()
    }, 1000);

    $("#crud").val("E");

});

$(document).on("click",".btn-hapus",function(){
    let current_row = $(this).parents('tr'); 
    if (current_row.hasClass('child')) { 
        current_row = current_row.prev(); 
    }
    let table = $('#table-customer').DataTable(); 
    let data = table.row( current_row).data();
    let idcate = data.idcat;
    var pad = $("#txtpadre").val(); 
    /*if( pad==0){
        swal({
            title: "HOLA",
            text: "HOLA",
            type: "warning",
           
        },)

    }else{*/
    swal({
        title: "Eliminar",
        text: "¿Desea eliminar la categoria?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },

    function(){

        let ajax = {
            method : "delete_customer",
            IDCategoria : idcate,
        }
        $.ajax({
            url:"administrador/api-categorias.php",
            type: "POST",
            data: ajax,
            success: function(data, textStatus, jqXHR)
            {
                $resp = JSON.parse(data);
                if($resp['status'] == true){
                    swal("Success delete customer");
                    let xtable = $('#table-customer').DataTable(); 
                    xtable.ajax.reload( null, false );
                }else{
                    swal("Error delete customer : "+$resp['message'])
                }
                
            },
            error: function (request, textStatus, errorThrown) {
                swal("Error ", request.responseJSON.message, "error");
            }
        });
    })/*}*/;
});


function add_customer(nm,pad){
    let ajax = {
        method: "new_customer",
        name : nm,
        padre: pad

    }
    $.ajax({
        url: "administrador/api-categorias.php",
        type: "POST",
        data: ajax,
        success: function(data, textStatus, jqXHR)
        {
            $resp = JSON.parse(data);
            if($resp['status'] == true){
                $("#txtname").val("");
                $("#txtpadre").val("");
                $("#txtcode").val("");
                $("#txtcode").focus();
                let xtable = $('#table-customer').DataTable(); 
                xtable.ajax.reload( null, false );
                swal("Success save new customer");
            }else{
                swal("Error save customer : "+$resp['message'])
            }
        },
        error: function (request, textStatus, errorThrown) {
            swal("Error ", request.responseJSON.message, "error");
        }
    });
}

function edit_customer(id,nm,pad){
    let ajax = {
        method: "edit_customer",
        IDCategoria :  id,
        Nombre_categoria : nm,
        IDPadre: pad

    }
    $.ajax({
        url:  "administrador/api-categorias.php",
        type: "POST",
        data: ajax,
        success: function(data, textStatus, jqXHR)
        {
            $resp = JSON.parse(data);

            if($resp['status'] == true){
                $("#modal-customer").modal("hide");
                swal("Success edit customer ");
                let xtable = $('#table-customer').DataTable(); 
                xtable.ajax.reload( null, false );
            }else{
                swal("Error save customer : "+$resp['message'])
            }
        },
        error: function (request, textStatus, errorThrown) {
            swal("Error ", request.responseJSON.message, "error");

        }
    });
}