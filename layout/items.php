<?php
$idp = $item['IDProducto'];
$resultado = $conn->connect()->prepare("SELECT * FROM imagen 
                                        WHERE  IDProducto = :idp");

$resultado->execute(['idp'=>$idp]);
$cont_slide = 0;
?>
<div class="col-sm-4 col-6">
  <div class="card tarjeta"> 
    <div id="carousel-<?php echo $idp;?>" class="carousel slide" data-ride="carousel">
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <?php
        while($imgs = $resultado->fetch(PDO::FETCH_ASSOC)){
          $active = "";
          if($cont_slide == 0){
            $active = "active";
          }
        ?>
        <div class="carousel-item <?php echo $active;?>">
          <img  class="img-responsive card-img-top"  
                data-trigger="hover" 
                height="217px" 
                src="dist/img/<?php echo $imgs['nombre_img'];?>">
        </div>
        <?php
          $cont_slide++;
        }
        ?>
      </div>
      <!-- Controls -->
      <a class="carousel-control-prev" href="#carousel-<?php echo $idp;?>" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next" href="#carousel-<?php echo $idp;?>" role="button" data-slide="next">
        <span class="carousel-control-next-icon"></span>
      </a>
    </div>
    
        <div class="card-body det">
            <span><b><?php echo $item['NombreProducto'];?></b></span></br>
            <div class="pal">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalProductos" 
                data-whatever="<?php echo $item['NombreProducto'];?>" data-dismiss="<?php echo $item['Descripcion'];?>">
                    Ver detalles
                </button>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="ModalProductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">             
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>                
</div>