<?php
include 'global/config.php';
include "global/sesiones.php";
include 'global/conexion.php';
include 'templates/cabecera.php';
include 'templates/sidebarAdm.php';
?>
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				USUARIOS
 			</h1>
		</section>
		<!-- Main content -->
		<section class="content">
			<!-- Default box -->
			<div class="box" style="background-color: white; padding: 1em">
				<div class="box-body">
					<div class="table-responsive margin">
						<table id="table-customer" class="table table-bordered">
							<thead>
								<tr>
									<th style="width: 10px">#ID</th>
									<th style="width: 40px">Nombre</th>
									<th style="width: 40px">RUT</th>
									<th style="width: 40px">Telefono</th>
									<th style="width: 40px">Correo</th>
									<th style="width: 40px">Comuna</th>
									<th style="width: 40px">Region</th>
									<th style="width: 50px;"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<!-- Modal -->
	<div class="modal" id="modal-customer" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title " id="myModalLabel"></h4>
				</div>
				<div class="modal-body">
					<div class="box-body">
						<div class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-9">
									<input type="hidden" id="crud">
									<input type="hidden" id="txtcode">
									<input type="text" class="form-control" id="txtname" placeholder="Nombre de la Categoria">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">IDPadre</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="txtpadre" placeholder="ID del Padre">
								</div>
							</div>

						</div>
					</div>						
				</div>
				<div class="modal-footer d-flex justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-primary" title="save button" id="btn-save"> <i class="fa fa-save"></i> Guardar</button>
				</div>
			</div>
		</div>
  </div>
  
  <?php
  include 'templates/piePagina.php';
  ?>
	<!-- Usuarios -->
	<script src="dist/js/usuarios.js"></script>


